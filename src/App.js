import React, { Component } from 'react';
import AddFormMessage from "./components/AddFormMessage/AddFormMessage";
import Chat from "./components/Chat/Chat";


class App extends Component {
state = {
    message: '',
    author: '',
    messages: []
};
     messageHandler = () => {
         clearInterval(this.interval);
         const url = 'http://146.185.154.90:8000/messages';
         const data = new URLSearchParams();
         data.set('message', this.state.message);
         data.set('author', this.state.author);

         fetch(url,{method: 'post', body: data,}).then(response => {
             if(response.ok) {
                 return response.json();
             }
        }).then(message => {
            const messages = [...this.state.messages];

             messages.push(message);
             this.setState({messages: messages})
         })
     };

    getMessage = () => {
        const date = this.state.messages[this.state.messages.length - 1].datetime;
    this.interval = setInterval( () => {
        fetch(`http://146.185.154.90:8000/messages?datetime=${date}`).then(response => {
            if(response.ok) {
                return response.json();
            }
        }).then(messages => {
            console.log(messages);
            this.setState({
                messages: [...this.state.messages].concat(messages)
            })

        });
      }, 2000)
};
     componentDidMount() {
         fetch('http://146.185.154.90:8000/messages').then(response => {
             if(response.ok) {
                 return response.json();
             }
         }).then(messages => {
             console.log(messages);
             this.setState({
                 messages: messages
             })

         });
     }

     componentDidUpdate() {
         clearInterval(this.interval);
         this.getMessage()
     }

    inputChange = (message) => {
      this.setState({ message})
    };

    inputAuthorHandler = (author) => {
        this.setState({author})
    };
  render() {
    return (
        <div>
          <AddFormMessage
              change={ this.inputChange} value={this.state.message}
              messageHandler={() => this.messageHandler}
              authorChange={this.inputAuthorHandler}
          />
            {this.state.messages.map((item, id) => (
                <Chat message={item.message} author={item.author} datetime={item.datetime} key={id} />
             ))}
        </div>
        )


  }
}

export default App;
