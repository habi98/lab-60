import React from 'react';
import './AddFormMessage.css'

const AddFormMessage = (props) => {
    return (
        <div className="form">
            <input className="input-message" type="text" placeholder="message" value={props.value} onChange={(e) =>  props.change(e.target.value)}/>
            <input className="input-author" type="text" placeholder="Author" onChange={(e) => props.authorChange(e.target.value)}/>
            <button type="button" className="btn" onClick={props.messageHandler()}>submit</button>
        </div>
    );
};

export default AddFormMessage;