import React from 'react';
import './Chat.css'
const Chat = (props) => {
    console.log(props);
    return (
        <div className="message">
            <p>{props.datetime}: [Author : {props.author}] {props.message}</p>
        </div>
    );
};

export default Chat;